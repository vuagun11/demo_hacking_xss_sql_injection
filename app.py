import MySQLdb
import MySQLdb.cursors
from flask import Flask, render_template, redirect, request

app = Flask(__name__)


def connect_sql():
    return MySQLdb.connect(host='127.0.0.1', user='root', passwd='123456', db='tung', port=3306,
                           charset='utf8', cursorclass=MySQLdb.cursors.DictCursor)


@app.route('/')
def index():
    return render_template('index.html')


# dung firefox
@app.route('/xss', methods=['POST', 'GET'])
def xss():
    query = request.form.get('s')
    fix = request.form.get('fix')
    return render_template('xss.html', query=query, fix=fix)


@app.route('/sql-injection', methods=['POST', 'GET'])
def sql_injection():
    if request.method == 'GET':
        return render_template('sql.html')
    elif request.method == 'POST':
        param = request.form.get('param')
        fix = request.form.get('fix')
        conn = connect_sql()
        cursor = conn.cursor()
        if fix:
            query = 'SELECT * FROM user WHERE department=%(id)s'
            cursor.execute(query, {'id': param})
        else:
            query = 'SELECT * FROM user WHERE department=' + param
            cursor.execute(query, {'id': param})
        users = cursor.fetchall()
        conn.close()
        return render_template('sql.html', users=users, param=param, fix=fix)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')
